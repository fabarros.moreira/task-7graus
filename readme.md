# Exercício OOP - BACKEND ROLE - Filipe Moreira

Este exercício consiste na criação de 3 classes, e são elas as seguintes:
 
 * Shape.php
 * Rectangle.php
 * Circle.php

As classes Rectangle e Circle herdam as propriedades e métodos da classe "Pai", a classe Shape.

## Shape

### Propriedades

 * name (string) - nome;
 * width (int) - largura;
 * length (int) - comprimento;
 * id (string) - identificador (único).

### Métodos

 * setName - Método para definir a propriedade nome;
 * getName - Método para obter a propriedade nome;
 * getId - Método para obter a propriedade id;
 * setId - Método para definir a propriedade id;
 * getShapeType - Método para obter a constante SHAPE_TYPE;
 * area - Método para calcular a área;
 * info - Método que retorna um objecto com as informações do mesmo;


## Rectangle

### Propriedades

 * name (string) - nome;
 * width (int) - largura;
 * length (int) - comprimento;
 * id (string) - identificador (único).

### Métodos

 * area - Método para calcular a área;
 * info - Método que retorna um objecto com as informações do mesmo;

## Circle

### Propriedades

 * name (string) - nome;
 * radius (int) - raio;
 * id (string) - identificador (único).

### Métodos

 * area - Método para calcular a área;
 * info - Método que retorna um objecto com as informações do mesmo;

## Exemplos

Para criar um objecto Rectangle ou Circle é necessário passar como parâmetros, a largura e o comprimento, o id é criado automaticamente, e o nome pode ser definido depois com o setName() (Era possível colocar no constructor e passar o nome também). 


```php
    $square = new Rectangle(5, 5);
    $square->setName('Quadrado');
```

Para obter o nome é necessário utilizar o método getName(), e para calcular a área o método area().

```php
    echo $square->getName();
    echo $square->area();
```

Para obter o objeto e as suas propriedades é necessário utilizar o método info().

```php
    echo $square->info();
```

