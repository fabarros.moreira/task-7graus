<?php
require_once('./Shape.php');

class Rectangle extends Shape{
    const SHAPE_TYPE = 2;
    
    function __construct($length, $width){
        parent::__construct($length, $width);
    }
    
    public function area() : int {        
        return $this->width * $this->length;
    }
    
    public function info(){
        $info = [
            'id' => $this->getId(),
            'name' => $this->name,
            'width' => $this->width,
            'length' => $this->length,
            'area' => $this->area()
        ];
        return json_encode($info);
    }
}