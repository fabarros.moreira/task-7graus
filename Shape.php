<?php
abstract class Shape {

    public string $name;
    protected int $length;
    protected int $width;    
    private string $id;
  
    const SHAPE_TYPE = 1;

    function __construct($length, $width){
        $this->setId();
        $this->length = $length;
        $this->width = $width;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getName() : string{
        return $this->name;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function setId(){
        $this->id = uniqid();
        return $this->id;
    }

    public static function getShapeType(){ 
        return self::SHAPE_TYPE; 
    }

    public abstract function area();
    
    public abstract function info();
}