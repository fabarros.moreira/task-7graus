<?php
require_once('./Shape.php');

class Circle extends Shape{
    const SHAPE_TYPE = 3; 

    protected int $radius;

    function __construct($radius)
    {
        $this->radius = $radius;
        parent::__construct(1, 1);
    }

    public function area() : float {
        return number_format($this->radius * $this->radius * M_PI, 2, '.', '');
    }
    
    public function info() {
        $info = [
            'id' => $this->getId(),
            'shape_type' => $this->getShapeType(),
            'name' => $this->name,
            'radius' => $this->radius,
            'area' => $this->area()
        ];
        return json_encode($info);
    }
}